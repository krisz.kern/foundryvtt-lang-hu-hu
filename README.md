# FoundryVTT lang hu-HU

## **HU**

Ez a modul elérhetővé teszi a Magyar nyelv kiválasztását a FoundryVTT beállításaiban.  
A nyelvbeállítás a program felületének különböző területeit magyarra fordítja.

### **Telepítés**

A FoundryVTT Modulok menüjében keress rá és telepítsd a **"Hungarian \[Core\]"** modult.

A FoundryVTT Konfiguráció menüjében válaszd ki a **"Magyar (Hungarian) - Hungarian \[Core\]"** _Alapértelmezett Nyelvet_.

Mentsd el a beállítást és indítsd újra a szervert! Készen is vagy.

---

## **EN**

This module adds the option to select the Hungarian language from the FoundryVTT settings menu.  
Selecting this option will translate various aspects of the program interface.

### **Installation**

In the FoundryVTT Add-On Modules menu search for and install the **"Hungarian \[Core\]"** module.

In the FoundryVTT Configuration menu choose the **"Magyar (Hungarian) - Hungarian \[Core\]"** _Default Language_.

Save the setting and restart the server! That's it.
